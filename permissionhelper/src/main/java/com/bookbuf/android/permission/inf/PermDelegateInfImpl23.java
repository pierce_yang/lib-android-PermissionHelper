package com.bookbuf.android.permission.inf;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.bookbuf.android.permission.PermMonitor;
import com.bookbuf.android.permission.PermUtil;
import com.bookbuf.android.permission.entity.MonitorActivityEntity;
import com.bookbuf.android.permission.entity.PermEntityCompat;

import java.util.Arrays;

/**
 * Created by robert on 16/5/27.
 */
public class PermDelegateInfImpl23 implements PermDelegateInf<PermEntityCompat> {

	protected final String TAG = "PermDelegateInfImpl23";

	public PermDelegateInfImpl23 () {
		Log.d (TAG, "getPermDelegateInfCompat: create permDelegateInf (>=23)");
	}

	@Override
	public void register (Activity activity, OnRequestPermissionsResultCallback<PermEntityCompat> callback) {
		PermMonitor.Box.clear ();
		PermMonitor.clear ();
		MonitorActivityEntity entity = new MonitorActivityEntity (activity, callback);
		Log.d (TAG, "register: create entity = " + entity.toString ());
		PermMonitor.addOrUpdate (entity);
	}

	@Override
	public void trace (PermEntityCompat... permEntityCompats) {
		final int traceId = PermMonitor.Box.put (permEntityCompats);
		Log.d (TAG, "trace: 新增跟踪 traceId = " + traceId);
		PermMonitor.Box.print ();
	}

	@Override
	public void request (PermEntityCompat... permEntityCompats) {
		String[] permissions = PermUtil.permissions (permEntityCompats);
		Log.d (TAG, "request: 待请求权限列表 = " + Arrays.toString (permEntityCompats));
		if (permissions != null) {
			MonitorActivityEntity entity = PermMonitor.get ();
			if (entity == null) {
				Log.e (TAG, "request: 没有注册回调 所以没法处理权限请求");
				return;
			}
			Log.d (TAG, "request: 默认调用栈顶的事件回调 " + entity.toString ());

			if (PermUtil.canShowRequestUI (entity, permEntityCompats)) {
				showRequestUI (entity, permEntityCompats);
			} else {
				PermUtil.request (entity, permEntityCompats);
			}
		}
	}

	private void showRequestUI (final MonitorActivityEntity entity, final PermEntityCompat... permEntityCompats) {
		StringBuilder title = new StringBuilder ();
		StringBuilder content = new StringBuilder ();
		if (permEntityCompats.length == 1) {
			PermEntityCompat single = permEntityCompats[0];
			title.append (single.getTitle ());
			content.append (single.getContent ());
		} else {
			for (PermEntityCompat permEntityCompat : permEntityCompats) {
				title.append (permEntityCompat.getTitle ());
				title.append (";");
				content.append (permEntityCompat.getContent ());
				content.append (";");
			}
		}
		Log.d (TAG, "showRequestUI: 显示请求UI");
		new AlertDialog.Builder (entity.getContainer ())
				.setTitle (title.toString ())
				.setMessage (content.toString ())
				.setPositiveButton (android.R.string.yes, new DialogInterface.OnClickListener () {
					public void onClick (DialogInterface dialog, int which) {
						PermUtil.request (entity, permEntityCompats);
					}
				})
				.setNegativeButton (android.R.string.no, new DialogInterface.OnClickListener () {
					public void onClick (DialogInterface dialog, int which) {
						Log.d (TAG, "onClick: 取消申请授权");
						final int traceId = PermMonitor.Box.filter (permEntityCompats);
						untrace (traceId);
					}
				})
				.show ();
	}

	@Override
	public boolean check (PermEntityCompat... permEntityCompats) {
		if (permEntityCompats != null) {
			/*挨个检查权限*/
			boolean granted = true;
			MonitorActivityEntity entity = PermMonitor.get ();
			granted &= PermUtil.check (entity, permEntityCompats);

			Log.e (TAG, "check: 回调权限检查结果:" + granted + "," + Arrays.toString (PermUtil.permissions (permEntityCompats)));
			/*处理回调结果*/
			OnRequestPermissionsResultCallback<PermEntityCompat> callback = (OnRequestPermissionsResultCallback<PermEntityCompat>) entity.getCallback ();
			callback.onChecked (granted, permEntityCompats);
			return granted;

		} else {
			/*不存在需要检查的权限*/
			Log.w (TAG, "check: 不存在需要检查的权限");
			return false;
		}
	}

	@Override
	public void parse (int traceId, String[] strings, int[] grants) {
		Log.e (TAG, "parse: traceId = " + traceId);
		Log.e (TAG, "parse: strings = " + Arrays.toString (strings));
		Log.e (TAG, "parse: grants = " + Arrays.toString (grants));
		PermMonitor.Box.print ();
		PermEntityCompat[] permEntityCompats = PermMonitor.Box.filter (traceId);
		if (permEntityCompats == null) {
			Log.w (TAG, "parse: 请求权限 与 返回结果列表数量不匹配[1]");
			return;
		} else if (strings.length != permEntityCompats.length) {
			Log.w (TAG, "parse: 请求权限 与 返回结果列表数量不匹配[2]");
		} else if (grants.length != permEntityCompats.length) {
			Log.w (TAG, "parse: 请求权限 与 返回结果列表数量不匹配[3]");
		}
		MonitorActivityEntity entity = PermMonitor.get ();
		boolean granted = PermUtil.check (entity, permEntityCompats);
		Log.w (TAG, "parse: 解析权限请求结果:" + granted);
		/*处理回调结果*/
		OnRequestPermissionsResultCallback<PermEntityCompat> callback = (OnRequestPermissionsResultCallback<PermEntityCompat>) entity.getCallback ();
		if (granted) {
			callback.onGranted (permEntityCompats);
		} else {
			callback.onDenied (permEntityCompats);
		}
	}

	@Override
	public void untrace (int traceId) {
		if (traceId != -1) {
			PermMonitor.Box.remove (traceId);
		}
		PermMonitor.Box.print ();
		Log.d (TAG, "untrace: 释放跟踪 traceId = " + traceId);
	}

	@Override
	public void unregister (Activity activity, OnRequestPermissionsResultCallback<PermEntityCompat> callback) {
		PermMonitor.Box.clear ();
		PermMonitor.clear ();
		Log.e (TAG, "unregister: ");
	}
}
